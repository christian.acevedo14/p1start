package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<Object> implements SortedList<Object> {

	private int currentSize;
	private Node<Object> header;
	private Comparator<Object> comparator;

	@SuppressWarnings("hiding")
	private class Node<Object> {
		Object NCar;
		Node<Object> prev, next;

		public Node(Object c) {
			NCar = c;
			prev = next = null;
		}

		public Node(Object c, Node<Object> p, Node<Object> n) {
			NCar = c;
			prev = p;
			next = n;
		}

		public void setNext(Node<Object> a) {
			this.next = a;
		}

		public void setPrev(Node<Object> pr) {
			this.prev = pr;
		}

		public Node<Object> getNext() {
			return this.next;
		}

		public Node<Object> getPrev() {
			return this.prev;
		}

		public Object getElement() {
			return this.NCar;
		}

		public void clear() {
			this.next = this.prev = null;
			this.NCar = null;
		}
	}

	private class CircularSortedDoublyLinkedIterator<Object> implements Iterator<Object> {

		private Node<Object> nextNode;

		public CircularSortedDoublyLinkedIterator() {

			this.nextNode = (Node<Object>) header.getNext();
		}

		@Override
		public boolean hasNext() {

			return nextNode != header;
		}

		@Override
		public Object next() {
			
				Object result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			 
		}

	}

	public CircularSortedDoublyLinkedList(Comparator<Object> c) {
		this.currentSize = 0;
		this.header = new Node<Object>(null);
		this.comparator = c;
	}

	@Override
	public Iterator<Object> iterator() {

		return new CircularSortedDoublyLinkedIterator<Object>();
	}

	@Override
	public boolean add(Object obj) {
		
		if (this.isEmpty()) {
			Node<Object> newCar = new Node<Object>(obj, header, header);
			this.header.setNext(newCar);
			this.header.setPrev(newCar);
			this.currentSize++;
			return true;
		} else {
			
			Node<Object> temp = header.getNext();
			for (int i = 0; i < this.currentSize; i++) { 
				if (this.comparator.compare(obj, temp.getElement()) <= 0) {
					Node<Object> nCar = new Node<Object>(obj, temp.getPrev(), temp);
					temp.getPrev().setNext(nCar);
					temp.setPrev(nCar);
					this.currentSize++;
					return true;
				} else
					temp = temp.getNext();
			}

			Node<Object> newNode = new Node<Object>(obj, header.getPrev(), header);
			header.getPrev().setNext(newNode);
			header.setPrev(newNode);
			this.currentSize++;
			return true;

		}
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(Object obj) {

		if (!this.contains(obj))
			return false;
		return this.remove(this.firstIndex(obj));
	}

	@Override
	public boolean remove(int index) {

		if (index < 0 || index >= this.currentSize)
			throw new IndexOutOfBoundsException();

		int count = 0;
		Node<Object> carrNode = header.getNext();
		Node<Object> prv, nxt;
		while (count != index) {
			carrNode = carrNode.getNext();
			count++;
		}
		prv = carrNode.getPrev();
		nxt = carrNode.getNext();
		prv.setNext(nxt);
		nxt.setPrev(prv);
		carrNode.clear();
		this.currentSize--;
		return true;
	}

	@Override
	public int removeAll(Object obj) {

		int count = 0;
		while (this.contains(obj)) {
			remove(this.firstIndex(obj));
			count++;
		}
		return count;
	}

	@Override
	public Object first() {
		return this.header.getNext().getElement();
	}

	@Override
	public Object last() {
		return this.header.getPrev().getElement();
	}

	@Override
	public Object get(int index) {
		if (index < 0 || index >= this.currentSize)
			throw new IndexOutOfBoundsException();

		int count = 0;
		Node<Object> c = header.getNext();

		while (count != index) {
			c = c.getNext();
			count++;
		}

		return c.getElement();
	}

	@Override
	public void clear() {
		while (this.currentSize != 0) {
			remove(0);
		}
	}

	@Override
	public boolean contains(Object e) {

		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}

	@Override
	public int firstIndex(Object e) {
		int index = 0;
		Iterator<Object> it = this.iterator();
		Object nCar = it.next();

		while (it.hasNext() && nCar != e) {
			nCar = it.next();
			index++;
		}
		if (nCar.equals(e))
			return index;
		else 
			return -1;
	}

	@Override
	public int lastIndex(Object e) {
		if (!this.contains(e))
			return -1;

		Node<Object> n = this.header.getNext(); 
		int lIndex = -1;
		for (int i =0 ; i< this.currentSize; i++) {
			if(n.getElement().equals(e)) lIndex = i;
			n = n.getNext();
		}
		return lIndex;
	}

}