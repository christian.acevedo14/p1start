package edu.uprm.cse.datastructures.cardealer;

import java.util.Iterator;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;  

@Path("/cars")

public class CarManager {

	private final SortedList<Car> cars = CarList.getInstance();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] returnCars() {
		Car[] nCars = new Car[cars.size()];
		
		/*
		 * int i = 0; Iterator<Car> it = cars.iterator(); while(it.hasNext()) { nCars[i]
		 * = it.next(); i++; }
		 */
		
		for (int i = 0; i < nCars.length; i++) {
			nCars[i] = cars.get(i);
		}
		
		return nCars;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car returnCar(@PathParam ("id") long id) {
		
		if(cars.isEmpty()) {
	        throw new NotFoundException();     
		}
		
//		 int i = 0;
//	 while(cars.get(i).getCarId() != id) { 
//			 i++;
//		 }
//		 
//		 if(cars.get(i).getCarId() == id) { 
//			 return cars.get(i);
//		 }
		 
		
		for(int j = 0; j < cars.size(); j++) {
			if(cars.get(j).getCarId() == id) {
				return cars.get(j);
			}
		}
		
	    throw new NotFoundException();
		   
	}
	


	@POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCustomer(Car car){
      cars.add(car);
      return Response.status(201).build();
    }  
    
    @PUT
    @Path("/{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCustomer(Car car, @PathParam("id") long id){
    
    	if(cars.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();      
    	} 
    	
    	int i = 0;
    	while(cars.get(i).getCarId() != id) {
    		i++;
    	}
      
      if (cars.get(i).getCarId() == id) {
    	  
    	  cars.remove(i);
    	  cars.add(car);
    	  
        return Response.status(Response.Status.OK).build();
      } else {
        return Response.status(Response.Status.NOT_FOUND).build();      
      }
    }   
    
    
    @DELETE
    @Path("/{id}/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCustomer(@PathParam("id") long id){
    	
    	if(cars.isEmpty()) {
            return Response.status(404).build();      
    	}
    	
    	int i = 0;
    	
    	while(cars.get(i).getCarId() != id) {
    		i++;
    	}
    	
    	if(cars.get(i).getCarId() == id) {
    		cars.remove(i);
    		
            return Response.status(200).build();
    		
    	}
    	
    	
    	else{
            return Response.status(404).build();      
      }
    }  
	
}
