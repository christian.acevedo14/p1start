package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	private static CircularSortedDoublyLinkedList<Car> R = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return R;
	}
	
	public static void resetCars() {
		R.clear();
	}
}
