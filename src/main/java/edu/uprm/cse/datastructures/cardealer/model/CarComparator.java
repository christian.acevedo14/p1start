package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
	
	@Override
	public int compare(Car c1,Car c2){
		if(c1.concat().equals(null)) {
			return -1;
		}
		if(c1.concat().equals(c2.concat())) {
			return 1;
		}
		
		return c1.concat().compareTo(c2.concat());
	}
	
	

}
